<?php
$nilai = 70;
$presensi = 90;

if ($nilai >= 80 && $presensi >= 80) {
    echo "Nilai Anda A";
} else if ($nilai >= 70 && $presensi >= 70) {
    echo "Nilai Anda B";
} else if ($nilai >= 60 && $presensi >= 60) {
    echo "Nilai Anda C";
} elseif ($nilai >= 50 && $presensi >= 50) {
    echo "Nilai Anda D";
} else {
    echo "Nilai Anda E";
}
