<?php

echo <<<KATA1
Berikut ini adalah contoh string multiline
menggunakan Heredoc, cara ini efektif untuk menampilkan
tulisan yang cukup panjang<br>
KATA1;


echo <<<'KATA2'
Berikut ini adalah contoh string multiline
menggunakan Nowdoc, cara ini efektif untuk menampilkan
tulisan yang cukup panjang
KATA2;
