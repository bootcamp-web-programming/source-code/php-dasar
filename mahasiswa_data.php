<?php
define("LAKI_LAKI", "Laki-Laki");
const PEREMPUAN = "Perempuan";
$mahasiswa = [
    [
        "nim" => "121220851",
        "nama" => "Andi Dharmawan",
        "gender" => LAKI_LAKI,
        "umur" => 23
    ],
    [
        "nim" => "121220852",
        "nama" => "Santi Eka Pratiwi",
        "gender" => PEREMPUAN,
        "umur" => 20
    ],
    [
        "nim" => "121220853",
        "nama" => "Ria Yuniar",
        "gender" => PEREMPUAN,
        "umur" => 19
    ],
    [
        "nim" => "121220854",
        "nama" => "Eko Purnomo",
        "gender" => LAKI_LAKI,
        "umur" => 22
    ]
];
echo "File Mahasiswa Data";
