<?php

function nilai($nilai, $presensi)
{
    if ($nilai >= 80 && $presensi >= 80) {
        return "A";
    } else if ($nilai >= 70 && $presensi >= 70) {
        return "B";
    } else if ($nilai >= 60 && $presensi >= 60) {
        return "C";
    } elseif ($nilai >= 50 && $presensi >= 50) {
        return "D";
    } else {
        return "E";
    }
}

echo "NIM : 121220851<br>";
echo "Nama : Andi Dharmawan<br>";
echo "Nilai : " . nilai(75, 90);
echo "<br>============================<br>";
echo "NIM : 121220852<br>";
echo "Nama : Santi Eka Pratiwi<br>";
echo "Nilai : " . nilai(85, 80);
