<?php
echo "While <br>";
$counter = 1;
while ($counter < 1) {
    echo "Perulangan ke - " . $counter . "<br>";
    $counter++;
}

echo "<br>Do While<br>";
$counter = 1;
do {
    echo "Perulangan ke - " . $counter . "<br>";
    $counter++;
} while ($counter < 1);
