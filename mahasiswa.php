<?php
include_once('mahasiswa_data.php');
?>
<table border="1" cellpadding="10" cellspacing="0">
    <thead>
        <tr>
            <th>Key</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($mahasiswa as $key => $mhs) { ?>
            <tr>
                <td><?php echo $key ?></td>
                <td><?php echo $mhs["nim"] ?></td>
                <td><?php echo $mhs["nama"] ?></td>
                <td><a href="mahasiswa_detail.php?id=<?= $key ?>">detail</a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>