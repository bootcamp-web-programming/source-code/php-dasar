<?php
$values = array(1, 2, 3, 4);
var_dump($values);

echo "<br>";

$kegiatan = ["Bootcamp", "Aplikasi", "Programming"];
var_dump($kegiatan);

echo "<br>";
var_dump($kegiatan[1]);
$kegiatan[1] = "Web";

echo "<br>";
var_dump($kegiatan);

echo "<br>";
$kegiatan[] = "PHP";
var_dump($kegiatan);
echo "<br>";
var_dump(count($kegiatan));
