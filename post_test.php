<?php
$balok = [
    ["P" => 10, "L" => 10, "T" => 10],
    ["P" => 10, "L" => 6, "T" => 4],
    ["P" => 15, "L" => 8, "T" => 6],
    ["P" => 25, "L" => 5, "T" => 10],
    ["P" => 32, "L" => 30, "T" => 28]
];

function keliling($panjang, $lebar, $tinggi)
{
    return 4 * ($panjang + $lebar + $tinggi);
}
function luas($panjang, $lebar, $tinggi)
{
    return 2 * ($panjang * $lebar + $panjang * $tinggi + $lebar * $tinggi);
}
function volume($pjg, $lbr, $tgi)
{
    return $pjg * $lbr * $tgi;
}
?>

<table border="1" cellpadding="10" cellspacing="0">
    <thead>
        <tr>
            <th>Panjang (P)</th>
            <th>Lebar (L)</th>
            <th>Tinggi (T)</th>
            <th>Keliling</th>
            <th>Luas</th>
            <th>Volume</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($balok as $blk) : ?>
            <tr>
                <td><?= $blk["P"] ?></td>
                <td><?= $blk["L"] ?></td>
                <td><?= $blk["T"] ?></td>
                <td><?= keliling($blk["P"], $blk["L"], $blk["T"]) ?></td>
                <td><?= luas($blk["P"], $blk["L"], $blk["T"]) ?></td>
                <td><?= volume($blk["P"], $blk["L"], $blk["T"]) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>