<?php
$nilai = "B";

switch ($nilai):
    case "A":
        echo "Anda Lulus dengan predikat Sangat Baik";
        break;
    case "B":
        echo "Anda Lulus dengan predikat Baik";
        break;
    case "C":
        echo "Anda Lulus dengan predikat Cukup";
        break;
    case "D":
        echo "Anda Tidak Lulus dengan predikat Buruk";
        break;
    default:
        echo "Anda Tidak Lulus dengan predikat Sangat Buruk";
        break;
endswitch;
