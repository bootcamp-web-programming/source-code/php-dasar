<?php
$makul = [
    [
        "nama" => "Dasar Pemrograman",
        "sks" => 4,
        "nilai" => 85
    ],
    [
        "nama" => "Web Programming",
        "sks" => 4,
        "nilai" => 80
    ],
    [
        "nama" => "Pancasila",
        "sks" => 3,
        "nilai" => 95
    ],
    [
        "nama" => "PTIK",
        "sks" => 3,
        "nilai" => 75
    ]
];

function grade($nilai)
{
    if ($nilai >= 80 && $nilai <= 100) {
        return "A";
    } elseif ($nilai >= 68 && $nilai <= 79) {
        return "B";
    } elseif ($nilai >= 56 && $nilai <= 67) {
        return "C";
    } elseif ($nilai >= 31 && $nilai <= 55) {
        return "D";
    } else {
        return "E";
    }
}
?>

NIM : 121220851<br>
Nama : Andi Dharmawan
<table border="1" cellpadding="10" cellspacing="0">
    <tr>
        <th>Mata Kuliah</th>
        <th>SKS</th>
        <th>Nilai</th>
        <th>Grade</th>
        <th>SKS x Bobot</th>
        <th>Predikat</th>
    </tr>
    <?php foreach ($makul as $mkl) { ?>
        <tr>
            <td><?= $mkl["nama"] ?></td>
            <td><?= $mkl["sks"] ?></td>
            <td><?= $mkl["nilai"] ?></td>
            <td>
                <?php echo grade($mkl["nilai"]) ?>
            </td>
            <td>
                <?php
                if ($mkl["nilai"] >= 80 && $mkl["nilai"] <= 100) {
                    echo $mkl["sks"] * 4;
                } elseif ($mkl["nilai"] >= 68 && $mkl["nilai"] <= 79) {
                    echo $mkl["sks"] * 3;
                } elseif ($mkl["nilai"] >= 56 && $mkl["nilai"] <= 67) {
                    echo $mkl["sks"] * 2;
                } elseif ($mkl["nilai"] >= 31 && $mkl["nilai"] <= 55) {
                    echo $mkl["sks"] * 1;
                } else {
                    echo $mkl["sks"] * 0;
                }
                ?>
            </td>
            <td>
                <?php
                if ($mkl["nilai"] >= 80 && $mkl["nilai"] <= 100) {
                    echo "Sangat Baik";
                } elseif ($mkl["nilai"] >= 68 && $mkl["nilai"] <= 79) {
                    echo "Baik";
                } elseif ($mkl["nilai"] >= 56 && $mkl["nilai"] <= 67) {
                    echo "Cukup";
                } elseif ($mkl["nilai"] >= 31 && $mkl["nilai"] <= 55) {
                    echo "Buruk";
                } else {
                    echo "Sangat Buruk";
                }
                ?>
            </td>
        </tr>
    <?php } ?>
</table>