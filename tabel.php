<?php
$nim1 = "121220851";
$nama1 = "Andi Dharmawan";
$gender1 = "Laki-Laki";
$umur1 = 23;

$nim2 = "121220852";
$nama2 = "Santi Eka Pratiwi";
$gender2 = "Perempuan";
$umur2 = 20;
?>
<table border="1" cellpadding="10" cellspacing="0">
    <thead>
        <tr>
            <th>NIM</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Umur</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $nim1 ?></td>
            <td><?php echo $nama1 ?></td>
            <td><?php echo $gender1 ?></td>
            <td><?php echo $umur1 ?></td>
        </tr>
        <tr>
            <td><?php echo $nim2 ?></td>
            <td><?php echo $nama2 ?></td>
            <td><?php echo $gender2 ?></td>
            <td><?php echo $umur2 ?></td>
        </tr>
    </tbody>
</table>