<?php
define("LAKI_LAKI", "Laki-Laki");
define("PEREMPUAN", "Perempuan");
$mahasiswa = [
    [
        "nim" => "121220851",
        "nama" => "Andi Dharmawan",
        "gender" => LAKI_LAKI,
        "umur" => 23
    ],
    [
        "nim" => "121220852",
        "nama" => "Santi Eka Pratiwi",
        "gender" => PEREMPUAN,
        "umur" => 20
    ],
    [
        "nim" => "121220853",
        "nama" => "Ria Yuniar",
        "gender" => PEREMPUAN,
        "umur" => 19
    ],
    [
        "nim" => "121220854",
        "nama" => "Eko Purnomo",
        "gender" => LAKI_LAKI,
        "umur" => 22
    ]
];
?>
<table border="1" cellpadding="10" cellspacing="0">
    <thead>
        <tr>
            <th>NIM</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Umur</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($mahasiswa as $mhs) {
            echo "
                <tr>
                    <td>" . $mhs["nim"] . "</td>
                    <td>" . $mhs["nama"] . "</td>
                    <td>" . $mhs["gender"] . "</td>
                    <td>" . $mhs["umur"] . "</td>
                </tr>
            ";
        }
        ?>
        <?php foreach ($mahasiswa as $mhs) { ?>
            <tr>
                <td><?php echo $mhs["nim"] ?></td>
                <td><?php echo $mhs["nama"] ?></td>
                <td><?php echo $mhs["gender"] ?></td>
                <td><?php echo $mhs["umur"] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>