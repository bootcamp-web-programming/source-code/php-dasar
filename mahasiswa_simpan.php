<?php
$nim = $_POST['nim'];
$nama = $_POST['nama'];
$gender = $_POST['gender'] == 'L' ? 'Laki-Laki' : 'Perempuan';
$umur = $_POST['umur'];
?>
<table cellpadding="3" cellspacing="0">
    <tr>
        <td>NIM</td>
        <td>: <?= $nim ?></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>: <?= $nama ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>: <?= $gender ?></td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>: <?= $umur ?></td>
    </tr>
    <tr>
        <td colspan="2"><a href="mahasiswa_add.php">Kembali</a></td>
    </tr>
</table>