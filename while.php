<?php

$counter = 1;

while ($counter <= 10) {
    echo "Perulangan Ke - " . $counter . "<br>";
    $counter++;
}

echo "======================================<br>";

while ($counter >= 1) :
    echo "Perulangan Ke - " . $counter . "<br>";
    $counter--;
endwhile;
